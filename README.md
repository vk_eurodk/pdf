# README
## PDF Document Storage - Sample Application

### Installation

__Clone or download branch pdf at [the repository's main page](https://bitbucket.org/vk_eurodk/pdf/src/master/)__

Just select __pdf__ branch in branches drop-down and click __Clone__ or __...__ > __Download repository__ to get the files

### Docker setup

Project consists of 2 folders - __docker__ and __src__

#### Installation step 1 - build

* Install docker and docker-compose if not yet
* Open command prompt, go to __docker__ folder and enter __docker-compose__ __up__
* Wait for images to build and __php-fpm__ image to output __NOTICE: ready to handle connections__

#### Installation step 2 - init

* Open one more command prompt, go to __docker__ folder and enter __docker-compose exec php-fpm php artisan initdb__
* Execution of the database initialization should pass quickly and without any messages

### Using the sample app

* Open __[http://localhost/](http://localhost/)__ in some browser. You should get empty 1st page of documents list.
* Test, if application's functionality meets requirements using small pdf files located in __src/sample_files__
* You may test it using larger files, but their processing probably can be aborted due to usual 30s timeout

### Launching the only end-to-end test - uploading 2 different files to check that upload works and __last__ file is added to the __beginning__ of the documents list

* Install last stable build of Node.js if not yet
* Open command prompt, go to project's __src__ folder and enter __npm__ __install__
* Wait for installation of Cypress and its dependencies to complete
* Staying at __src__ folder launch Cypress test runner GUI by entering __npx__ __cypress__ __open__
* Launch __functional/uploading.spec.js__ from Cypress GUI window
* Watch test running if your computer is slow to prevent timeouts taking snapshots
* Test should pass

You can launch this test several times if you want to populate the document list automatically

### Continuous Integration

* StyleCI does not update repository as frequently as needed and proposes changes that violate PSR-2 as far as I know - this is why I cannot warrant that its results are correct as long as I am limited by the free plan
* Scrutinizer works fine. Its results can be seen in pdf pull request's pipeline that is usually available on the sideboard of __[the diff page](https://bitbucket.org/vk_eurodk/pdf/pull-requests/3/pdf/diff)__ but due to firm 6s overall timeout for Cypress tests I could not launch them on Scrutinizer

### To solve problems or provide feedback contact me

### Who do I talk to? ###

Valerijs Krizevics

__+371 29548048__

__[vkrizevics@gmail.com](mailto:vkrizevics@gmail.com)__